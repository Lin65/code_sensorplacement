# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 21:20:18 2015

@author: xiaolin
"""

from DispersionModel import *
import pickle
from simulation_setting import *
import matplotlib.pyplot as plt
from utils import *

#------------------------------------------------------------------------------
# initial samples
#------------------------------------------------------------------------------
initial_release_smps = np.tile(release_range[0], (1, num_smps)) + np.tile(release_range[1] - release_range[0], (1, num_smps))*np.random.rand(1, num_smps)
initial_wind_dir_smps = np.tile(wind_dir_range[0], (1, num_smps)) + np.tile(wind_dir_range[1], (1, num_smps))*np.random.randn(1, num_smps)
initial_smps = np.vstack((initial_release_smps, initial_wind_dir_smps))

with open('initial_smps.pickle', 'w') as i:
    pickle.dump(initial_smps, i)

plt.figure()
plt.hist(initial_smps[0],100)
plt.title('prior distribution of possible release location')

plt.figure()
plt.hist(initial_smps[1],100)
plt.title('prior distribution of possible wind direction')

plt.figure()
plt.plot(initial_smps[0], initial_smps[1], '.')
plt.title('joint distribution of initial samples')


#------------------------------------------------------------------------------
# run simulations
#
# c_dispersion_list_grids = [ [ array1, array2,...] , [array1, array2,...] , [array1, array2,...], ..., [array1, array2,...] ]
#                time step:      1st     2nd             1st    2nd             1st     2nd                1st      2nd
#                samples:             1st                    2nd                     3rd                      num_smps
#------------------------------------------------------------------------------
c_dispersion_list_grids = [0]*num_smps

for i in range(0, num_smps):
    c_dispersion_list_grids[i] = dispersion(mass, real_wind_spd, initial_smps[1][i],p, q, initial_smps[0][i], start_time_sequence, time_sequence, time_interval, x_grid, y_grid)
    for j in range(0, num_steps):
        c_dispersion_list_grids[i][j] = simulateMeasMult( c_dispersion_list_grids[i][j], logmu, logstd, 2)

with open('c_dispersion_list_grids.pickle', 'w') as f:
    pickle.dump(c_dispersion_list_grids, f)
    