# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 22:44:13 2015

@author: xiaolin
"""

from DispersionModel import *

from utils import *
import numpy as np
import pickle
import matplotlib.pyplot as plt
from simulation_setting import *


with open('c_dispersion_list_grids.pickle') as c:
    c_dispersion_list_grids = pickle.load(c)
    
with open('first_sensor_location.pickle') as f:
    x_max_indices_1, y_max_indices_1, Loc_1_x, Loc_1_y = pickle.load(f)

with open('initial_smps.pickle') as i:
    initial_smps = pickle.load(i)

#------------------------------------------------------------------------------
# select second sensor position
#------------------------------------------------------------------------------

my_chain_2 = np.zeros((num_smps, (num_steps-1)*2 + 2)) 
my_split_2 = [(num_steps-1)*2,2]
MI_map_2 = x_grid*0

for i in range(0, y_resolution):
    for j in range(0, x_resolution):
        print "computing mutual information at (i, j):   ", (i,j)
        row = 0
        for r in range(0, num_smps):
            d_list = []
            for t in range(1, num_steps):               
                d_list.append(c_dispersion_list_grids[r][t][x_max_indices_1[0]][y_max_indices_1[0]])
            for t in range(1, num_steps): 
                d_list.append(c_dispersion_list_grids[r][t][i][j])               
            d_list.append(initial_smps[0][r]) 
            d_list.append(initial_smps[1][r])
            my_chain_2[row] = np.array(d_list)
            row = row + 1
        MI_map_2[i][j] = MI_CCA(my_chain_2, my_split_2)
        
        
# to avoid nan        
for i in range(0, y_resolution):
    for j in range(0, x_resolution):
        if MI_map_2[i][j]== -1e10:
            MI_map_2[i][j] = 1e10
            
MI_map_2_min = MI_map_2.min()
      
for i in range(0, y_resolution):
    for j in range(0, x_resolution):
        if MI_map_2[i][j] == 1e10:
            MI_map_2[i][j] = MI_map_2_min

MI_map_2[x_max_indices_1[0]][y_max_indices_1[0]] = 0.5*(MI_map_2[x_max_indices_1[0]-1][y_max_indices_1[0]] + MI_map_2[x_max_indices_1[0]+1][y_max_indices_1[0]])


with open('MI_map_2.pickle', 'w') as m:
    pickle.dump(MI_map_2, m)
    
[x_max_indices_2, y_max_indices_2] = np.where(MI_map_2 == MI_map_2.max())
Loc_2_x = x_axis[y_max_indices_2[0]]
Loc_2_y = y_axis[x_max_indices_2[0]]

with open('second_sensor_location.pickle', 'w') as s:
    pickle.dump([x_max_indices_2, y_max_indices_2, Loc_2_x, Loc_2_y], s)

plt.figure()
plt.pcolor(x_grid, y_grid, MI_map_2, cmap='rainbow', vmin=MI_map_2.min(), vmax=MI_map_2.max())
plt.colorbar()
plt.title('second sensor')
plt.scatter(np.array([Loc_1_x, Loc_2_x]), np.array([Loc_1_y, Loc_2_y]),s = 60, c = 'b', marker = 'D')
# set the limits of the plot to the limits of the data
plt.axis([x_grid.min(), x_grid.max(), y_grid.min(), y_grid.max()])
plt.savefig('second_sensor.eps',format='eps', dpi=1200)

import third_sensor_selection
