# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 09:21:29 2015

@author: xiaolin
"""

import numpy as np
from scipy.stats import norm
from scipy.stats import lognorm
import matplotlib.pyplot as plt


x = np.linspace(0.5,2,1000)

pdf_log = lognorm.pdf(x, 0.1, loc = -0.005)
pdf_norm = norm.pdf(x, loc = 1, scale = 0.1)

plt.figure()
plt.plot(x, pdf_log, 'r', markersize=2, label='Approximation')
plt.plot(x, pdf_norm, label='Expected distribution')
plt.legend(loc="upper right", shadow=True, fancybox=True)
#plt.axis([0, 48, average_ENT.min()-2, average_ENT.max()+2])
plt.xlabel("x")
plt.ylabel("pdf")
plt.savefig('obs.eps',format='eps', dpi=1200)
plt.savefig('obs.pdf',format='pdf', dpi=1200)
