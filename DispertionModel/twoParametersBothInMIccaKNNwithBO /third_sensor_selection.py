# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 22:54:49 2015

@author: xiaolin
"""
from DispersionModel import *

from utils import *
import numpy as np
import pickle
import matplotlib.pyplot as plt
from simulation_setting import *

with open('c_dispersion_list_grids.pickle') as c:
    c_dispersion_list_grids = pickle.load(c)
    
with open('first_sensor_location.pickle') as f:
    x_max_indices_1, y_max_indices_1, Loc_1_x, Loc_1_y = pickle.load(f)
    
with open('second_sensor_location.pickle') as s:
    x_max_indices_2, y_max_indices_2, Loc_2_x, Loc_2_y = pickle.load(s)
    
with open('initial_smps.pickle') as i:
    initial_smps = pickle.load(i)
    
#------------------------------------------------------------------------------
# select third sensor position
#------------------------------------------------------------------------------
my_chain_3 = np.zeros((num_smps, (num_steps-1)*3 + 2)) 
my_split_3 = [(num_steps-1)*3,2]
MI_map_3 = x_grid*0

for i in range(0, y_resolution):
    for j in range(0, x_resolution):
        print "computing mutual information at (i, j):   ", (i,j)
        row = 0
        for r in range(0, num_smps):
            d_list = []
            for t in range(1, num_steps):               
                d_list.append(c_dispersion_list_grids[r][t][x_max_indices_1[0]][y_max_indices_1[0]])
            for t in range(1, num_steps):                               
                d_list.append(c_dispersion_list_grids[r][t][x_max_indices_2[0]][y_max_indices_2[0]])
            for t in range(1, num_steps):      
                d_list.append(c_dispersion_list_grids[r][t][i][j])
            d_list.append(initial_smps[0][r])
            d_list.append(initial_smps[1][r])
            my_chain_3[row] = np.array(d_list)
            row = row + 1
        MI_map_3[i][j] = MI_CCA(my_chain_3, my_split_3)

# to avoid nan        
for i in range(0, y_resolution):
    for j in range(0, x_resolution):
        if MI_map_3[i][j]== -1e10:
            MI_map_3[i][j] = 1e10
            
MI_map_3_min = MI_map_3.min()
      
for i in range(0, y_resolution):
    for j in range(0, x_resolution):
        if MI_map_3[i][j] == 1e10:
            MI_map_3[i][j] = MI_map_3_min
        
MI_map_3[x_max_indices_1[0]][y_max_indices_1[0]] = 0.5*(MI_map_3[x_max_indices_1[0]+1][y_max_indices_1[0]] + MI_map_3[x_max_indices_1[0]-1][y_max_indices_1[0]]) 
MI_map_3[x_max_indices_2[0]][y_max_indices_2[0]] = 0.5*(MI_map_3[x_max_indices_2[0]+1][y_max_indices_2[0]] + MI_map_3[x_max_indices_2[0]-1][y_max_indices_2[0]]) 

with open('MI_map_3.pickle', 'w') as tm:
    pickle.dump(MI_map_3, tm)
    
[x_max_indices_3, y_max_indices_3] = np.where(MI_map_3 == MI_map_3.max())
Loc_3_x = x_axis[y_max_indices_3[0]]
Loc_3_y = y_axis[x_max_indices_3[0]]

with open('third_sensor_location.pickle', 'w') as t:
    pickle.dump([x_max_indices_3, y_max_indices_3, Loc_3_x, Loc_3_y], t)

plt.figure()
plt.pcolor(x_grid, y_grid, MI_map_3, cmap='rainbow', vmin=MI_map_3.min(), vmax=MI_map_3.max())
plt.colorbar()
plt.title('third sensor')
plt.scatter(np.array([Loc_1_x, Loc_2_x, Loc_3_x]), np.array([Loc_1_y, Loc_2_y, Loc_3_y]),s = 60, c = 'b', marker = 'D')
# set the limits of the plot to the limits of the data
plt.axis([x_grid.min(), x_grid.max(), y_grid.min(), y_grid.max()])

plt.savefig('third_sensor.eps',format='eps', dpi=1200)

     

