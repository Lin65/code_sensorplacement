# -*- coding: utf-8 -*-
"""
Created on Wed Aug 26 11:42:32 2015

@author: xiaolin
"""

from utils import *
import numpy as np
import pickle
import matplotlib.pyplot as plt
from simulation_setting import *
from DispersionModel import *
import copy 


    
with open('initial_smps.pickle') as i:
    initial_smps = pickle.load(i)
    
#-------------------------------------------------------------------------
# selected sensor locations    
#-------------------------------------------------------------------------
with open('first_sensor_location.pickle') as f:
    x_max_indices_1, y_max_indices_1, Loc_1_x, Loc_1_y = pickle.load(f)
    
with open('second_sensor_location.pickle') as s:
    x_max_indices_2, y_max_indices_2, Loc_2_x, Loc_2_y = pickle.load(s)
    
with open('third_sensor_location.pickle') as t:
    x_max_indices_3, y_max_indices_3, Loc_3_x, Loc_3_y = pickle.load(t)

selected_sensor_locations = [np.array([x_max_indices_1[0],y_max_indices_1[0]]), np.array([x_max_indices_2[0],y_max_indices_2[0]]), np.array([x_max_indices_3[0],y_max_indices_3[0]])]

#-------------------------------------------------------------------------
# generate sensor locations list
#-------------------------------------------------------------------------


sensor_locations_list = [0]*num_comp
sensor_locations_list[0] = selected_sensor_locations
num_sensors = 3

for i in range(1, num_comp):
    sensor_locations_list[i] = sensor_locations_generator(y_resolution, x_resolution, num_sensors)

'''
with open('sensor_locations_list.pickle', 'w') as sensor:
    pickle.dump(sensor_locations_list, sensor)        
'''
#-------------------------------------------------------------------------
# generate measurement data
#-------------------------------------------------------------------------
meas_dispersion_list_grids = dispersion(mass, real_wind_spd, real_wind_dir,p, q, real_release_source, start_time_sequence, time_sequence, time_interval, x_grid, y_grid)
for j in range(0, num_steps):
    meas_dispersion_list_grids[j] = meas_dispersion_list_grids[j] + obs_std*np.random.randn(y_resolution, x_resolution) 

meas_data_list_list = [0]*num_comp # store meas_data_list for each combination
# meas_data_list = [ meas_data, meas_data, meas_data ,...]  at each time step 
meas_data_list = [0]*num_steps
    
for j in range(0, num_comp):
    for i in range(0, num_steps):
        meas1 = meas_dispersion_list_grids[i][sensor_locations_list[j][0][0]][sensor_locations_list[j][0][1]]
        meas2 = meas_dispersion_list_grids[i][sensor_locations_list[j][1][0]][sensor_locations_list[j][1][1]]
        meas3 = meas_dispersion_list_grids[i][sensor_locations_list[j][2][0]][sensor_locations_list[j][2][1]]
        meas_data_list[i] = np.array([meas1, meas2, meas3])  
        
    meas_data_list_list[j] = copy.deepcopy(meas_data_list)
    
    
    
    
#------------------------------------------------------------------------------
# update ensembles using observation data on three sensors
#------------------------------------------------------------------------------    
H = np.array([[0,0,1,0,0],[0,0,0,1,0],[0,0,0,0,1]])
R = np.array([[obs_std**2, 0., 0.],[0., obs_std**2, 0.],[0., 0., obs_std**2]])

X_list_list = [0]*num_comp # store x_list for each combination x_list_list = [x_list, x_list,....]
ENT_list_list = [0]*num_comp 

for i in range(0, num_comp):
    result = completeEnKFupdate(sensor_locations_list[i], meas_data_list_list[i], initial_smps, H, R, num_steps, num_smps, start_time_sequence, time_sequence, time_interval, x_grid, y_grid, mass, real_wind_spd, p, q)   
    X_list = result[0]
    ENT_list = result[1] 
    X_list_list[i] = copy.deepcopy(X_list)
    ENT_list_list[i] = copy.deepcopy(ENT_list)

'''        
with open('X_list_list.pickle', 'w') as x:
    pickle.dump(X_list_list, x)
with open('ENT_list_list.pickle', 'w') as E:
    pickle.dump(ENT_list_list, E)        
'''
# plot entropy of release location at each time step
plt.figure()
plt.plot(time_sequence, ENT_list)
plt.title('entropy of release location at each time step')


# plot distribution of ensembles of releaselocation

plt.figure()
plt.subplot(2,3,1)
plt.hist(X_list_list[0][0][0,:],100)
plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'x')
plt.locator_params(nbins=4)

plt.title('prior')

plt.subplot(2,3,2)
plt.hist(X_list_list[0][3][0,:],100)
plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'x')
plt.locator_params(nbins=4)


plt.title('the 3rd update')

plt.subplot(2,3,3)
plt.hist(X_list_list[0][6][0,:],100)
plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'x')
plt.locator_params(nbins=4)

plt.title('the 6th update ')

plt.subplot(2,3,4)
plt.hist(X_list_list[0][9][0,:],100)
plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'x')
plt.locator_params(nbins=4)

plt.title('the 9th update ')

plt.subplot(2,3,5)
plt.hist(X_list_list[0][12][0,:],100)
plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'x')
plt.locator_params(nbins=4)


plt.title(' the 12th update ')

plt.subplot(2,3,6)
plt.hist(X_list_list[0][16][0,:],100)
plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'x')
plt.locator_params(nbins=4)

plt.title('the 16th update ')

plt.tight_layout() 

plt.savefig('EnKFupdate.eps',format='eps', dpi=1200)
plt.savefig('EnKFupdate.pdf',format='pdf', dpi=1200)
# plot distribution of ensembles of winddirection

plt.figure()
plt.subplot(2,3,1)
plt.hist(X_list_list[0][0][1,:],100)
plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'x')
plt.locator_params(nbins=4)

plt.title(' initial ensembles')

plt.subplot(2,3,2)
plt.hist(X_list_list[0][3][1,:],100)
plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'x')
plt.locator_params(nbins=4)

plt.title(' the 3rd update')

plt.subplot(2,3,3)
plt.hist(X_list_list[0][6][1,:],100)
plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'x')
plt.locator_params(nbins=4)

plt.title(' the 6th update ')

plt.subplot(2,3,4)
plt.hist(X_list_list[0][9][1,:],100)
plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'x')
plt.locator_params(nbins=4)

plt.title(' the 9th update ')

plt.subplot(2,3,5)
plt.hist(X_list_list[0][12][1,:],100)
plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'x')
plt.locator_params(nbins=4)

plt.title('the 12th update ')

plt.subplot(2,3,6)
plt.hist(X_list_list[0][16][1,:],100)
plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'x')
plt.locator_params(nbins=4)

plt.title(' the 16th update ')
plt.tight_layout() 



