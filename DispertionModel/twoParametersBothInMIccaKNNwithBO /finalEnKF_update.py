# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 21:30:15 2015

@author: xiaolin
"""


from utils import *
import numpy as np
import pickle
#import matplotlib.pyplot as plt
from simulation_setting import *
from DispersionModel import *
import copy
    
with open('initial_smps.pickle') as i:
    initial_smps = pickle.load(i)
    
    
'''
#-------------------------------------------------------------------------
# selected sensor locations    
#-------------------------------------------------------------------------
with open('first_sensor_location.pickle') as f:
    x_max_indices_1, y_max_indices_1, Loc_1_x, Loc_1_y = pickle.load(f)
    
with open('second_sensor_location.pickle') as s:
    x_max_indices_2, y_max_indices_2, Loc_2_x, Loc_2_y = pickle.load(s)
    
with open('third_sensor_location.pickle') as t:
    x_max_indices_3, y_max_indices_3, Loc_3_x, Loc_3_y = pickle.load(t)

selected_sensor_locations = [np.array([x_max_indices_1[0],y_max_indices_1[0]]), np.array([x_max_indices_2[0],y_max_indices_2[0]]), np.array([x_max_indices_3[0],y_max_indices_3[0]])]

#-------------------------------------------------------------------------
# generate sensor locations list
#-------------------------------------------------------------------------

sensor_locations_list = [0]*num_comp
sensor_locations_list[0] = selected_sensor_locations
num_sensors = 3

for i in range(1, num_comp):
    sensor_locations_list[i] = sensor_locations_generator(y_resolution, x_resolution, num_sensors)

with open('sensor_locations_list.pickle', 'w') as sensor:
    pickle.dump(sensor_locations_list, sensor)   

print "sensor locations done !"
'''

with open('sensor_locations_list.pickle') as sensor:
    sensor_locations_list = pickle.load(sensor) 

real_release_source_list = initial_smps[0][100:(100 + num_cases)]
real_wind_dir_list = initial_smps[1][100:(100 + num_cases)]

X_list_list_list = [0]*num_cases
ENT_list_list = [0]*num_cases
ENTboth_list_list = [0]*num_cases

for icase in range(0, num_cases):
    print "case ", icase
#-------------------------------------------------------------------------
# generate measurement data [ [array([meas1, meas2, meas3]), array([meas1, meas2, meas3]), ...] , [], [], ...]
#-------------------------------------------------------------------------
    meas_dispersion_list_grids = dispersion(mass, real_wind_spd, real_wind_dir_list[icase],p, q, real_release_source_list[icase], start_time_sequence, time_sequence, time_interval, x_grid, y_grid)
    for j in range(0, num_steps):
        meas_dispersion_list_grids[j] = simulateMeasMult(meas_dispersion_list_grids[j], logmu, logstd, 2)
        

    meas_data_list_list = [0]*num_comp # store meas_data_list for each combination
    # meas_data_list = [ meas_data, meas_data, meas_data ,...]  at each time step 
    meas_data_list = [0]*num_steps
    
    for j in range(0, num_comp):
        for i in range(0, num_steps):
            meas1 = meas_dispersion_list_grids[i][sensor_locations_list[j][0][0]][sensor_locations_list[j][0][1]]
            meas2 = meas_dispersion_list_grids[i][sensor_locations_list[j][1][0]][sensor_locations_list[j][1][1]]
            meas3 = meas_dispersion_list_grids[i][sensor_locations_list[j][2][0]][sensor_locations_list[j][2][1]]
            meas_data_list[i] = np.array([meas1, meas2, meas3])
        
        meas_data_list_list[j] = copy.deepcopy(meas_data_list)
    
#------------------------------------------------------------------------------
# update ensembles using observation data on three sensors
#------------------------------------------------------------------------------    
    H = np.array([[0,0,1,0,0],[0,0,0,1,0],[0,0,0,0,1]])
    R = np.array([[logstd**2, 0., 0.],[0., logstd**2, 0.],[0., 0., logstd**2]])

    X_list_list = [0]*num_comp # store x_list for each combination x_list_list = [x_list, x_list,....]
    ENT_list = [0]*num_comp 
    ENTboth_list = [0]*num_comp 
    
    for i in range(0, num_comp):
        print "    ",i, "  comp "
        result = completeEnKFupdate(sensor_locations_list[i], meas_data_list_list[i], initial_smps, H, R, num_steps, num_smps, start_time_sequence, time_sequence, time_interval, x_grid, y_grid, mass, real_wind_spd, p, q)   
        X_list = result[0]
        ENT = result[1]
        ENTboth = result[2]
        X_list_list[i] = copy.deepcopy(X_list)
        ENT_list[i] = copy.deepcopy(ENT)
        ENTboth_list[i] = copy.deepcopy(ENTboth)
    
    if (icase ==0):
        X_list_list_list[icase] = copy.deepcopy(X_list_list)
    ENT_list_list[icase] = copy.deepcopy(ENT_list)
    ENTboth_list_list[icase] = copy.deepcopy(ENTboth_list)

        
with open('X_list_list_list.pickle', 'w') as xlist:
    pickle.dump(X_list_list_list, xlist)
with open('ENT_list_list.pickle', 'w') as ENTlist:
    pickle.dump(ENT_list_list, ENTlist)        
with open('ENTboth_list_list.pickle', 'w') as ENTbothlist:
    pickle.dump(ENTboth_list_list, ENTbothlist)   
