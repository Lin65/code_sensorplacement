# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 03:37:45 2015

@author: xiaolin
"""

from DispersionModel import *
import matplotlib.pyplot as plt
from pylab import *


mass = 5e5
wind_spd = 5.
wind_dir = 1.73031176e-01
release_source = -1163.4810784
time_sequence = np.linspace(0,900, 6)
time_interval = time_sequence[1] - time_sequence[0]
start_time = 0  # start_time should be in time_sequence !
start_time_sequence = np.linspace(0,360,3)

p = 0.466
q = 0.866

x_axis = np.linspace(0, 10000, 11)
y_axis = np.linspace(-10000, 10000, 21)

[x_grid, y_grid] = np.meshgrid(x_axis, y_axis)

c_list_grids = puff(mass, wind_spd, wind_dir,p, q, release_source, start_time, time_sequence, time_interval, x_grid, y_grid)


plt.figure()
plt.subplot(2, 2, 1)
plt.ylabel('y (km)')
plt.contour(x_grid/1000, y_grid/1000, c_list_grids[1])
#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'both')
plt.locator_params(nbins=4)
plt.title("t = 3 min")

plt.subplot(2, 2, 2)
plt.contour(x_grid/1000, y_grid/1000, c_list_grids[3])
#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'both')
plt.locator_params(nbins=4)
plt.title("t = 24 min")

plt.subplot(2, 2, 3)
plt.xlabel('x (km)')
plt.ylabel('y (km)')
plt.contour(x_grid/1000, y_grid/1000, c_list_grids[4])
#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'both')
plt.locator_params(nbins=4)
plt.title("t = 36 min")

plt.subplot(2, 2, 4)
plt.xlabel('x (km)')
plt.contour(x_grid/1000, y_grid/1000, c_list_grids[5])

#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'both')
plt.locator_params(nbins=4)
plt.title("t = 48 min")
plt.tight_layout() 

plt.savefig('first_puff.eps',format='eps', dpi=1200)
plt.savefig('first_puff.pdf',format='pdf', dpi=1200)

c_dispersion_list_grids = dispersion(mass, wind_spd, wind_dir,p, q, release_source, start_time_sequence, time_sequence, time_interval, x_grid, y_grid)

plt.figure()
plt.subplot(2, 2, 1)
plt.ylabel('y (km)')
plt.contour(x_grid/1000, y_grid/1000, c_dispersion_list_grids[1])
#plt.pcolor(x_grid, y_grid, c_dispersion_list_grids[1], cmap='Blues', vmin = 0, vmax = 1.226)

#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'both')
plt.locator_params(nbins=4)
plt.title("t = 3 min")

plt.subplot(2, 2, 2)
plt.contour(x_grid/1000, y_grid/1000, c_dispersion_list_grids[3])
#plt.pcolor(x_grid, y_grid, c_dispersion_list_grids[8], cmap='Blues', vmin = 0, vmax = 1.226)

#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'both')
plt.locator_params(nbins=4)
plt.title("t = 24 min")

plt.subplot(2, 2, 3)
plt.xlabel('x (km)')
plt.ylabel('y (km)')
#levels = np.linspace(0, c_dispersion_list_grids[12].max(), 1000)
plt.contour(x_grid/1000, y_grid/1000, c_dispersion_list_grids[4])
#plt.pcolor(x_grid, y_grid, c_dispersion_list_grids[12], cmap='Blues', vmin = 0, vmax = 1.226)
#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'both')
plt.locator_params(nbins=4)
plt.title("t = 36 min")

plt.subplot(2, 2, 4)
plt.xlabel('x (km)')
plt.contour(x_grid/1000, y_grid/1000, c_dispersion_list_grids[5])
#plt.pcolor(x_grid, y_grid, c_dispersion_list_grids[16], cmap='Blues', vmin = 0, vmax = 1.226)

#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'both')
#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'both')
plt.locator_params(nbins=4)
plt.title("t = 48 min")
plt.tight_layout() 

plt.savefig('real_dispersion.eps',format='eps', dpi=1200)
plt.savefig('real_dispersion.pdf',format='pdf', dpi=1200)
