# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 21:21:50 2015

@author: xiaolin
"""
from DispersionModel import *

from utils import *
import numpy as np
import pickle
import matplotlib.pyplot as plt
from simulation_setting import *


    
with open('initial_smps.pickle') as i:
    initial_smps = pickle.load(i)

#------------------------------------------------------------------------------
# select first sensor position
#------------------------------------------------------------------------------

[BO_Loc_1_x, BO_Loc_1_y] = BayesOpt_MI(domain, funcMI_BO, initial_smps, num_smps,num_steps, logmu, logstd, mass, real_wind_spd,p, q, start_time_sequence, time_sequence, time_interval, [], 1)


with open('BO_first_sensor_location.pickle', 'w') as f:
    pickle.dump([BO_Loc_1_x, BO_Loc_1_y], f)
    
#import second_sensor_selection