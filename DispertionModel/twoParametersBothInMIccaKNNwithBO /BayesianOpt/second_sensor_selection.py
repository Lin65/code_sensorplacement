# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 22:44:13 2015

@author: xiaolin
"""

from DispersionModel import *

from utils import *
import numpy as np
import pickle
import matplotlib.pyplot as plt
from simulation_setting import *



with open('BO_first_sensor_location.pickle') as f:
    Loc_1_x, Loc_1_y = pickle.load(f)

with open('initial_smps.pickle') as i:
    initial_smps = pickle.load(i)

#------------------------------------------------------------------------------
# select second sensor position
#------------------------------------------------------------------------------

PreviousLoc = [Loc_1_x, Loc_1_y]
SensorIndex = 2

[BO_Loc_2_x, BO_Loc_2_y] = BayesOpt_MI(domain, funcMI_BO, initial_smps, num_smps,num_steps, logmu, logstd, mass, real_wind_spd,p, q, start_time_sequence, time_sequence, time_interval, PreviousLoc, SensorIndex)



with open('BO_second_sensor_location.pickle', 'w') as s:
    pickle.dump([BO_Loc_2_x, BO_Loc_2_y], s)

import third_sensor_selection