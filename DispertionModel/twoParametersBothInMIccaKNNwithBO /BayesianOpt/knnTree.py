import numpy as np
import scipy.special 
import sys
from sklearn.neighbors import NearestNeighbors
import copy

    
#--------------------------------------------------------------------------
# Program returns the number of points within my_dist
#--------------------------------------------------------------------------        
def knnDist(queryMatrix, dataMatrix, my_dists):
    
    numPts = np.zeros(queryMatrix.shape[0])
    
    numDataVectors = dataMatrix.shape[0]
    numQueryVectors = queryMatrix.shape[0]
        
    for i in range(0, numQueryVectors):
        
        diffMat = np.tile(queryMatrix[i,:], (numDataVectors, 1)) - dataMatrix;

        dist = np.amax(abs(diffMat), axis = 1)
       
        numPts[i] = np.size(np.where(dist <= my_dists[i]))
    
    return numPts

#--------------------------------------------------------------------------
# Program to compute entropy using KNN method 
#--------------------------------------------------------------------------        
def knnENT(chain, my_knn, dim):
    
    my_chain = copy.deepcopy(chain)
    lenChain = float(my_chain.shape[0])
    dimChain = float(dim)

    if dimChain ==1:
        my_chain = np.array([my_chain]).T
        
    nbrs = NearestNeighbors(n_neighbors = my_knn+1, algorithm='kd_tree',metric='chebyshev').fit(my_chain)
    distToK, indices = nbrs.kneighbors(my_chain)
    distOfK = distToK[:,my_knn]
    
 #   for i in range(0, lenChain):
 #       if (distOfK[i]==0):
 #           distOfK[i] = 1
    
    logDist = np.log(2.*distOfK)

    ent = - scipy.special.psi(my_knn) + scipy.special.psi(lenChain) + dimChain/lenChain * np.sum(logDist)
    
    return ent

#--------------------------------------------------------------------------
# Program to compute mutual information using KNN method 
#--------------------------------------------------------------------------        
def knnMI(my_chain, my_knn, my_split):
    
    (lenChain, dimChain) = my_chain.shape
    my_dx = my_split[0]
    dim = my_split[0] + my_split[1]

    if(dim != dimChain):
        print "the joint dimension in the split is different from the dimension of the chain"
        sys.exit(1)
    
    # Normalize the chain
    mean_chain = np.mean(my_chain, axis = 0)
    std_chain = np.std(my_chain, axis = 0)
    
    my_chain = (my_chain - np.tile(mean_chain, (lenChain, 1)))/np.tile(std_chain, (lenChain, 1))
    
    # Estimate mutual information
    
    nbrs = NearestNeighbors(n_neighbors = my_knn+1, algorithm='kd_tree',metric='chebyshev').fit(my_chain)
    distToK, indices = nbrs.kneighbors(my_chain)
    distOfK = distToK[:,my_knn]
    
    numPts_x = knnDist(my_chain[:,0:my_dx], my_chain[:,0:my_dx], distOfK)
    numPts_y = knnDist(my_chain[:,my_dx:], my_chain[:,my_dx:], distOfK)
    
    mi = scipy.special.psi(my_knn) + scipy.special.psi(lenChain) - np.mean(scipy.special.psi(numPts_x + 1) + scipy.special.psi(numPts_y + 1))
      
    return mi        
   

        
    
    
        




















  