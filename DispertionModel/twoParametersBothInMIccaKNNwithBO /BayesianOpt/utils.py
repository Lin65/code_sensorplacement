# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 01:27:11 2015

@author: xiaolin
"""
import numpy as np
from DispersionModel import *
import copy
from sklearn.cross_decomposition import CCA
from BayesO import *

from knnTree import *

#---------------------------------------------------------------------------
# Entropy approximation    
#---------------------------------------------------------------------------  Tr
def ENT_GaussApprox(my_chain, my_d):
        
#--------------------------------------------------------------------------
# Normalize the chain
#--------------------------------------------------------------------------
    my_Sigma = np.cov(my_chain.T)
#--------------------------------------------------------------------------
# Estimate mutual information
#--------------------------------------------------------------------------
   # (sign, logdet) = np.linalg.slogdet(my_Sigma) 
    if my_d == 1:
        exactENT = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_d  * my_Sigma ))
    else:   
        exactENT = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_d  * np.linalg.det(my_Sigma) ))
        
    ent = exactENT
    return ent
#---------------------------------------------------------------------------
# mutual information approximation    
#---------------------------------------------------------------------------    
def MI_GaussApprox(my_chain_input,my_split):
    
    my_chain = copy.deepcopy(my_chain_input)
    [lenChain, dimChain] = my_chain.shape
    my_dx = my_split[0]
    my_dy = my_split[1]
    my_d = sum(my_split)

    if( my_d != dimChain ):
        print('ERROR: the joint dimension in the split is different from the dimension of the chain')
        import sys
        sys.exit(1)

#--------------------------------------------------------------------------
# Normalize the chain
#-------------------------------------------------------------------------
   
 #   mean_chain = np.mean(my_chain, axis = 0)
 #   std_chain = np.std(my_chain, axis = 0)   
 #   my_chain = (my_chain - np.tile(mean_chain, (lenChain, 1)))/np.tile(std_chain, (lenChain, 1))
    
    my_Sigma = np.cov(my_chain.T)
#--------------------------------------------------------------------------
# Estimate mutual information
#--------------------------------------------------------------------------
    my_Sigma_x = my_Sigma[0:my_dx][:,0:my_dx]
    my_Sigma_y = my_Sigma[my_dx:][:,my_dx:]
    
  #  my_Sigma_x = np.cov(my_chain[:, 0:my_dx].T)
  #  my_Sigma_y = np.cov(my_chain[:, my_dx:].T)
    
    (sign_x, logdet_x) = np.linalg.slogdet(my_Sigma_x) 
    (sign_y, logdet_y) = np.linalg.slogdet(my_Sigma_y) 
    (sign, logdet) = np.linalg.slogdet(my_Sigma) 
    exactENT_x = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_dx  ) ) + logdet_x
    exactENT_y = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_dy ) ) + logdet_y
    exactENT_xy = np.log( np.sqrt( (2*np.pi*np.exp(1))**my_d  ) ) + logdet

    exactMI = exactENT_x + exactENT_y - exactENT_xy

    mi = exactMI
    return mi


#---------------------------------------------------------------------------
# EnKF 
#
# [y1,        [0  0  1  0  0,       [ location,
#  y2,   =     0  0  0  1  0,  *      wind,                +  noise ~ N(0, R)
#  y3]         0  0  0  0  1]         concentration1,
#                                     concentration2,
#                                     concentration3]
#
#  X : 5*num_smps,   H: 3*5,   R = [obs_std^2  0           0
#                                   0          obs_std^2   0
#                                   0          0           obs_std^2]
#
#  meas_data: 1*3
#
#---------------------------------------------------------------------------

def EnKFupdate(X, H, R, meas_data):
    num_smps = X.shape[1]
    # Get the sampling covariance of states
    X_mu = np.mean(X, axis = 1)
    Ex = X - np.tile(X_mu, (num_smps, 1)).transpose()
    Pxx = 1./(num_smps -1) * np.dot(Ex, Ex.transpose())
 
    # Get the perturbed measurement covariance
    Ee = np.dot(np.linalg.cholesky(R).transpose() , np.random.randn(3, num_smps))
    Re = 1./(num_smps - 1) * np.dot(Ee, Ee.transpose())

    
    # tile the perturbed measurement
    meas_pert = np.tile(meas_data, (num_smps, 1)).transpose() + Ee
    
    # Update the ensembles
    PH = np.dot(Pxx, H.transpose())

    # avoid singular matrix
    addsmall = np.eye(np.shape(Re)[0])*1.0e-100
    INV = np.linalg.inv(np.dot(H, PH) + Re + addsmall)

    RES = meas_pert - np.dot(H, X) + 0.005

    X_update = X + np.dot(np.dot(PH, INV), RES)
    
    
    return X_update    
        
#--------------------------------------------------------------------------
# extract states from the map and update with EnKFupdate
#--------------------------------------------------------------------------
def completeEnKFupdate(x_location, y_location, meas_data_list, initial_smps, H, R, num_steps, num_smps, start_time_sequence, time_sequence, time_interval, mass, real_wind_spd, p, q):
#------------------------------------------------------------------------------
# create augmented states X list containing ensembles at every time step
#
#  X_list = [ X, X, X, ... ]  , X is an 5*num_smps array
#
#  X = [releaseLocation , ...
#       winddirection ,   ...
#       c1,               ...
#       c2,               ...
#       c3,               ...]
#------------------------------------------------------------------------------
    X_list = [0]*num_steps
    initialX = np.zeros((5, num_smps))

    # initial ensembles
    for i in range(0, num_smps):
        state1 = 0.
        state2 = 0.
        state3 = 0.
        initialX[:,i] = np.array([initial_smps[0][i], initial_smps[1][i], state1, state2, state3])
    
    X_list[0] = initialX
#---------------------------------------------------------------------------
# EnKF 
#
# [y1,        [0  0  1  0  0,       [ location,
#  y2,   =     0  0  0  1  0,  *      wind,                +  noise ~ N(0, R)
#  y3]         0  0  0  0  1]         concentration1,
#                                     concentration2,
#                                     concentration3]
#
#  X : 5*num_smps,   H: 3*5,   R = [obs_std^2  0           0
#                                   0          obs_std^2   0
#                                   0          0           obs_std^2]
#
#  meas_data: 1*3
#---------------------------------------------------------------------------

# EnKF update ensembles at each time step

    X = np.zeros((5, num_smps))
    ENT = np.linspace(0, 0,num_steps) # store entropy of release location
    ENT[0] = knnENT(X_list[0][0].T, 4,1)
    ENTboth = np.linspace(0, 0,num_steps) # store entropy of release location and wind direction
    ENTboth[0] = knnENT(X_list[0][0:2].T, 4,2)

    for i in range(0, num_steps-1):
        for j in range(0, num_smps):
            if len(start_time_sequence) > (i+1):
                start_time_subsequence = start_time_sequence[0:(i+2)]  # make sure they have same time interval
            else:
                start_time_subsequence = start_time_sequence
            time_subsequence = time_sequence[0:(i+2)]
            c_dispersion = dispersion(mass, real_wind_spd, X_list[i][1][j],p, q, X_list[i][0][j], start_time_subsequence, time_subsequence, time_interval, x_location, y_location)
            state1 = c_dispersion[i+1][0]
            state2 = c_dispersion[i+1][1]
            state3 = c_dispersion[i+1][2]
            X[:,j] = np.array([X_list[i][0][j], X_list[i][1][j], state1, state2, state3])
        # transform 
        for it in range(0, num_smps):
            X[0][it] = Logit((X[0][it] - (-3000))/(3000 - (-3000)))
            X[2][it] = np.log(X[2][it] + 1e-100) 
            X[3][it] = np.log(X[3][it] + 1e-100) 
            X[4][it] = np.log(X[4][it] + 1e-100) 
        
        X_list[i+1] = EnKFupdate(X, H, R, np.log(meas_data_list[i+1]))
        
        
        # transform back
        for ivt in range(0, num_smps):
            X_list[i+1][0][ivt] = -3000 + (3000 - (-3000))*InverseLogit(X_list[i+1][0][ivt])
            X_list[i+1][2][ivt] = np.exp(X_list[i+1][2][ivt])
            X_list[i+1][3][ivt] = np.exp(X_list[i+1][3][ivt])
            X_list[i+1][4][ivt] = np.exp(X_list[i+1][4][ivt])       
        # compute entropy
        my_chain = X_list[i+1][0].T
        ENT[i+1] = knnENT(my_chain, 4, 1)
        my_chain_both = X_list[i+1][0:2].T
        ENTboth[i+1] = knnENT(my_chain_both, 4, 2)
    
    result = [X_list, ENT, ENTboth]
    return result

#-----------------------------------------------------------------------
# generate sensor locations [ array([x1,y1]), array([x2,y2]), ...]
#-----------------------------------------------------------------------    
'''
def sensor_locations_generator(num_x_choices, num_y_choices, num_sensors):
    sensor_locations = [0]*num_sensors
    for i in range(0, num_sensors):
        sensor_locations[i] = np.array([np.random.choice(range(0,num_x_choices)), np.random.choice(range(0,num_y_choices))])
    
    return sensor_locations
'''        
def sensor_locations_generator(num_x_choices, num_y_choices, num_sensors):
    sensor_locations = [0]*num_sensors
    x_choice = np.random.choice(range(0,num_x_choices), num_sensors)
    y_choice = np.random.choice(range(0,num_y_choices), num_sensors)

    for i in range(0, num_sensors):
        sensor_locations[i] = np.array([x_choice[i],y_choice[i]])
    
    return sensor_locations         
 
#--------------------------------------------------------------------------
# observation with additive noise  dim = 2 means 2by2, 1means row vector, o means scaler
#--------------------------------------------------------------------------    
def simulateMeasAdd( cur_state, meas_std, dim):
    
    if (dim == 2):
        err_norm = np.random.normal(0, 1, (cur_state.shape[0], cur_state.shape[1]))
    elif (dim == 1):
        err_norm = np.random.normal(0, 1, (cur_state.shape[0]))
    elif (dim ==0 ):
        err_norm = np.random.normal(0 ,1)
    else:
        print "the state variabe has wrong dimention !"


    sim_meas = cur_state + meas_std * err_norm

    return sim_meas



#--------------------------------------------------------------------------
# observation with multiplicative noise dim = 2 means n by 2, 1means row vector, o means scaler
#--------------------------------------------------------------------------
def simulateMeasMult( cur_state, logmu, logstd, dim ):
    
    if (dim == 2):
        err_norm = np.random.normal(logmu, logstd, (cur_state.shape[0], cur_state.shape[1]))
    elif (dim == 1):
        err_norm = np.random.normal(logmu, logstd, (cur_state.shape[0]))
    elif (dim ==0 ):
        err_norm = np.random.normal(logmu, logstd)
    else:
        print "the state variabe has wrong dimention !"

    #err_norm = np.random.normal(logmu, logstd, (cur_state.shape[0], cur_state.shape[1]))


    log_sim_meas = np.log(cur_state + 1e-100) + err_norm
    sim_meas = np.exp(log_sim_meas)


    return sim_meas


#-------------------------------------------------------------------------
# logit for transformation of constraint varaible
#-------------------------------------------------------------------------    
def Logit(x):
    t = np.log(x/(1-x))
    return t
    
 #-------------------------------------------------------------------------
# inverseLogit for inverse transformation of constraint varaible
#-------------------------------------------------------------------------        
def InverseLogit(x):
    t = 1/(1 + np.exp(-x))
    return t    
    
#-------------------------------------------------------------------------
# cca mutual information
#-------------------------------------------------------------------------
def MI_CCA(my_chain_input, my_split):
    my_chain = copy.deepcopy(my_chain_input)
    my_dx = my_split[0]
    my_dy = my_split[1]
    my_dim = my_dx + my_dy
    
    cca = CCA(n_components=1)
    cca.fit(my_chain[:,0:my_dx], my_chain[:,my_dx:my_dim])
    #X_cc, Y_cc = cca.transform(samples_2[:,0:dim_x], samples_2[:,dim_x:dim])
    X_c = cca.x_scores_.T[0]
    Y_c = cca.y_scores_.T[0]
    XY = np.vstack((X_c, Y_c))
    if ((X_c.min() == X_c.max())or(Y_c.min()==Y_c.max())):
        MI_cca = -1e10
    else:
        #p = np.corrcoef(XY)
        #MI_cca = -0.5*np.log(1-p[0][1]**2.)
        MI_cca = knnMI(XY.T, 6, [1,1])
    
    return MI_cca

#---------------------------------------------------------------------------
# MI function to be used in Bayesian optimazition
#---------------------------------------------------------------------------
def funcMI_BO(x, y, samples, num_samples, num_steps,logmu, logstd, mass, real_wind_spd,p, q, start_time_sequence, time_sequence, time_interval,PreviousLoc, SensorIndex):
    
    if SensorIndex == 1:
        c_dispersion_list_grids = [0]*num_samples
        my_chain = np.zeros((num_samples, (num_steps-1) + 2))

        for i in range(0, num_samples):
            c_dispersion_list_grids[i] = dispersion(mass, real_wind_spd, samples[1][i],p, q, samples[0][i], start_time_sequence, time_sequence, time_interval, x, y)
            for j in range(0, num_steps):
                c_dispersion_list_grids[i][j] = simulateMeasMult( c_dispersion_list_grids[i][j], logmu, logstd, 0)

        row = 0
        for r in range(0, num_samples):
            d_list = []
            for t in range(1, num_steps):
                d_list.append(c_dispersion_list_grids[r][t])
            d_list.append(samples[0][r])
            d_list.append(samples[1][r])

            my_chain[row] = np.array(d_list)
            row = row + 1
        MI_est = MI_CCA(my_chain, [(num_steps-1), 2])
        
    if SensorIndex == 2:
        Loc_x_array = np.array([PreviousLoc[0], x])
        Loc_y_array = np.array([PreviousLoc[1], y])
        
        c_dispersion_list_grids = [0]*num_samples
        my_chain = np.zeros((num_samples, (num_steps-1)*2 + 2))
        
        for i in range(0, num_samples):
            c_dispersion_list_grids[i] = dispersion(mass, real_wind_spd, samples[1][i],p, q, samples[0][i], start_time_sequence, time_sequence, time_interval, Loc_x_array, Loc_y_array)
            for j in range(0, num_steps):
                c_dispersion_list_grids[i][j] = simulateMeasMult( c_dispersion_list_grids[i][j], logmu, logstd, 1)
        
        row = 0
        for r in range(0, num_samples):
            d_list = []
            for t in range(1, num_steps):
                d_list.append(c_dispersion_list_grids[r][t][0])
            for t in range(1, num_steps):
                d_list.append(c_dispersion_list_grids[r][t][1])
            d_list.append(samples[0][r])
            d_list.append(samples[1][r])
            my_chain[row] = np.array(d_list)
            row = row + 1
        MI_est = MI_CCA(my_chain, [(num_steps-1)*2, 2])

    if SensorIndex == 3:
        Loc_x_array = np.array([PreviousLoc[0][0], PreviousLoc[1][0], x])
        Loc_y_array = np.array([PreviousLoc[0][1], PreviousLoc[1][1], y])
        
        c_dispersion_list_grids = [0]*num_samples
        my_chain = np.zeros((num_samples, (num_steps-1)*3 + 2))
        
        for i in range(0, num_samples):
            c_dispersion_list_grids[i] = dispersion(mass, real_wind_spd, samples[1][i],p, q, samples[0][i], start_time_sequence, time_sequence, time_interval, Loc_x_array, Loc_y_array)
            for j in range(0, num_steps):
                c_dispersion_list_grids[i][j] = simulateMeasMult( c_dispersion_list_grids[i][j], logmu, logstd, 1)

        row = 0
        for r in range(0, num_samples):
            d_list = []
            for t in range(1, num_steps):
                d_list.append(c_dispersion_list_grids[r][t][0])
            for t in range(1, num_steps):
                d_list.append(c_dispersion_list_grids[r][t][1])
            for t in range(1, num_steps):
                d_list.append(c_dispersion_list_grids[r][t][2])
            d_list.append(samples[0][r])
            d_list.append(samples[1][r])
            my_chain[row] = np.array(d_list)
            row = row + 1
        MI_est = MI_CCA(my_chain, [(num_steps-1)*3, 2])


    return MI_est

#---------------------------------------------------------------------------
# Bayesian optimazition
#---------------------------------------------------------------------------

def BayesOpt_MI(domain, funcMI_BO, samples, num_samples,num_steps, logmu, logstd, mass, real_wind_spd,p, q, start_time_sequence, time_sequence, time_interval, PreviousLoc, SensorIndex):
    
    def funcMI(x , y):
        return funcMI_BO(x, y, samples, num_samples, num_steps, logmu, logstd, mass, real_wind_spd,p, q, start_time_sequence, time_sequence, time_interval, PreviousLoc, SensorIndex)

    bo = BayesianOptimization(funcMI, {'x': (domain[0][0], domain[0][1]), 'y':(domain[1][0], domain[1][1]) })
    bo.explore({'x': [1000., 5000.,9000.,1000.,5000., 9000.], 'y': [-8000.,-8000.,-8000.,8000., 8000., 8000.]})
    bo.maximize(n_iter=30)
    print(bo.res['max'])
    print bo.res['max']['max_params']
    
    return [bo.res['max']['max_params']['x'], bo.res['max']['max_params']['y']]





