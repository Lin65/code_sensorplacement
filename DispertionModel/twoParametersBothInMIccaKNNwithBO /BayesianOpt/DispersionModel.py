# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 02:40:47 2015

@author: xiaolin
"""

import numpy as np

# mass = 10, wind_spd = 1, wind_dir = 0, release_source = scalar , time_sequence = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10], time_interval =1

def puff(mass, wind_spd, wind_dir,p, q, release_source, start_time, time_sequence, time_interval, x_grid, y_grid):
    num_steps = len(time_sequence)
    x_center = np.zeros((1, num_steps))
    y_center = np.zeros((1, num_steps))
    D_center = np.zeros((1, num_steps))
    sigma_radius = np.zeros((1, num_steps))
    c_list_grids = [0]*num_steps  # each element will be a concentration map
    
    for i in range(0, num_steps):
        if (time_sequence[i] < start_time) or (time_sequence[i] == start_time):
            c_list_grids[i] = x_grid * 0.
            x_center[0][i] = 0.
            y_center[0][i] = release_source
            D_center[0][i] = 0.
            c_list_grids[i] = x_grid*0.
        else:
            x_center[0][i] = x_center[0][i-1] + wind_spd*np.cos(wind_dir)*time_interval
            y_center[0][i] = y_center[0][i-1] + wind_spd*np.sin(wind_dir)*time_interval
            D_center[0][i] = D_center[0][i-1] + wind_spd*time_interval
            sigma_radius[0][i] = p*D_center[0][i]**q
            c_list_grids[i] = mass/(2*np.pi*sigma_radius[0][i]**2)*np.exp(-((x_center[0][i] - x_grid)**2 + (y_center[0][i] - y_grid)**2)/(2*sigma_radius[0][i]**2))
    
    return c_list_grids
    
def dispersion(mass, wind_spd, wind_dir,p, q, release_source, start_time_sequence, time_sequence, time_interval, x_grid, y_grid):
    num_steps = len(time_sequence)
    c_list_grids_list = [0]*num_steps       
    c_dispersion_list_grids = [] # each element will be a concentration map of the summation of all the puffs
    num_puffs = len(start_time_sequence)
    for i in range(0, num_puffs):
        c_list_grids_list[i] = puff(mass, wind_spd, wind_dir,p, q, release_source, start_time_sequence[i], time_sequence, time_interval, x_grid, y_grid)
    
    for j in range(0, num_steps):
        sum = c_list_grids_list[0][0] * 0.
        for i in range(0, num_puffs):
            sum = sum + c_list_grids_list[i][j]
        c_dispersion_list_grids.append(sum)
    
    return c_dispersion_list_grids
    
           

    

