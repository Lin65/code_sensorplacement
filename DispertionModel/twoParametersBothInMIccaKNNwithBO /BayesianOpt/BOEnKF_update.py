# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 21:30:15 2015

@author: xiaolin
"""


from utils import *
import numpy as np
import pickle
#import matplotlib.pyplot as plt
from simulation_setting import *
from DispersionModel import *
import copy
    
with open('initial_smps.pickle') as i:
    initial_smps = pickle.load(i)
    
    

#-------------------------------------------------------------------------
# selected sensor locations    
#-------------------------------------------------------------------------
with open('BO_first_sensor_location.pickle') as f:
    Loc_1_x, Loc_1_y = pickle.load(f)
    
with open('BO_second_sensor_location.pickle') as s:
    Loc_2_x, Loc_2_y = pickle.load(s)
    
with open('BO_third_sensor_location.pickle') as t:
    Loc_3_x, Loc_3_y = pickle.load(t)

x_location = np.array([Loc_1_x, Loc_2_x, Loc_3_x])
y_location = np.array([Loc_1_y, Loc_2_y, Loc_3_y])



'''
#-------------------------------------------------------------------------
# generate sensor locations list
#-------------------------------------------------------------------------

sensor_locations_list = [0]*num_comp
sensor_locations_list[0] = selected_sensor_locations
num_sensors = 3

for i in range(1, num_comp):
    sensor_locations_list[i] = sensor_locations_generator(y_resolution, x_resolution, num_sensors)

with open('sensor_locations_list.pickle', 'w') as sensor:
    pickle.dump(sensor_locations_list, sensor)   

print "sensor locations done !"
'''
'''
with open('sensor_locations_list.pickle') as sensor:
    sensor_locations_list = pickle.load(sensor) 
'''
real_release_source_list = initial_smps[0][100:(100 + num_cases)]
real_wind_dir_list = initial_smps[1][100:(100 + num_cases)]

X_list_list = [0]*num_cases
ENT_list = [0]*num_cases
ENTboth_list = [0]*num_cases

for icase in range(0, num_cases):
    print "case ", icase
#-------------------------------------------------------------------------
# generate measurement data [ [array([meas1, meas2, meas3]), array([meas1, meas2, meas3]), ...] , [], [], ...]
#-------------------------------------------------------------------------
    meas_data_list = dispersion(mass, real_wind_spd, real_wind_dir_list[icase],p, q, real_release_source_list[icase], start_time_sequence, time_sequence, time_interval, x_location, y_location)
    for j in range(0, num_steps):
        meas_data_list[j] = simulateMeasMult(meas_data_list[j], logmu, logstd, 1)

    
#------------------------------------------------------------------------------
# update ensembles using observation data on three sensors
#------------------------------------------------------------------------------    
    H = np.array([[0,0,1,0,0],[0,0,0,1,0],[0,0,0,0,1]])
    R = np.array([[logstd**2, 0., 0.],[0., logstd**2, 0.],[0., 0., logstd**2]])
    
    result = completeEnKFupdate(x_location, y_location, meas_data_list, initial_smps, H, R, num_steps, num_smps, start_time_sequence, time_sequence, time_interval, mass, real_wind_spd, p, q)
    X_list = result[0]
    ENT = result[1]
    ENTboth = result[2]
    
    if (icase ==0):
        X_list_list[icase] = copy.deepcopy(X_list)
    ENT_list[icase] = copy.deepcopy(ENT)
    ENTboth_list[icase] = copy.deepcopy(ENTboth)

        
with open('BO_X_list_list.pickle', 'w') as xlist:
    pickle.dump(X_list_list, xlist)
with open('BO_ENT_list.pickle', 'w') as ENTlist:
    pickle.dump(ENT_list, ENTlist)
with open('BO_ENTboth_list.pickle', 'w') as ENTbothlist:
    pickle.dump(ENTboth_list, ENTbothlist)
