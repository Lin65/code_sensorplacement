# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 21:19:16 2015

@author: xiaolin
"""

import numpy as np

#------------------------------------------------------------------------------
# prior information
#------------------------------------------------------------------------------
release_range = [-3000., 3000.] #uniform distribution
wind_dir_range = [0., np.pi/18] #Gaussian distribution N(0, pi/18^2)
num_para = 2

#------------------------------------------------------------------------------
# observation noise
#------------------------------------------------------------------------------
obs_std = 1e-5 #variance of the additive observation noise

logmu = -0.005  # mean of multiplicative observation noise
logstd = 0.1   # std deviation of multiplicative observation noise

#------------------------------------------------------------------------------
# samples in Monte Carlo methods
#------------------------------------------------------------------------------
num_smps =1000

#------------------------------------------------------------------------------
# for conditional entropy / final comparison
#------------------------------------------------------------------------------
num_comp = 20
num_cases = 50

'''
initial_release_smps = np.tile(release_range[0], (1, num_smps)) + np.tile(release_range[1], (1, num_smps))*np.random.rand(1, num_smps)
initial_wind_dir_smps = np.tile(wind_dir_range[0], (1, num_smps)) + np.tile(wind_dir_range[1], (1, num_smps))*np.random.randn(1, num_smps)
initial_smps = np.vstack((initial_release_smps, initial_wind_dir_smps))

plt.figure()
plt.hist(initial_smps[0],100)
plt.title('prior distribution of possible release location')

plt.figure()
plt.hist(initial_smps[1],100)
plt.title('prior distribution of possible wind direction')
'''

#------------------------------------------------------------------------------
# real parameters
#------------------------------------------------------------------------------
mass = 5e5
real_wind_spd = 4.
real_wind_dir = -0.1235022
real_release_source = 2560.2737959
p = 0.466
q = 0.866

#------------------------------------------------------------------------------
# time setting
#------------------------------------------------------------------------------
time_sequence = np.linspace(0,1800, 31)
time_interval = time_sequence[1] - time_sequence[0]
start_time_sequence = np.linspace(0,600,11 )
num_steps = len(time_sequence)

#------------------------------------------------------------------------------
# domain
#------------------------------------------------------------------------------
x_resolution = 11
y_resolution = 21
x_axis = np.linspace(0, 10000, x_resolution)
y_axis = np.linspace(-10000, 10000, y_resolution)
[x_grid, y_grid] = np.meshgrid(x_axis, y_axis)
domain = np.array([[0.,10000.],[-10000.,10000.]])