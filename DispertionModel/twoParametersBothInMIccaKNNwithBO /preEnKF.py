# -*- coding: utf-8 -*-
"""
Created on Tue Sep  8 22:27:51 2015

@author: xiaolin
"""

from utils import *
import numpy as np
import pickle
#import matplotlib.pyplot as plt
from simulation_setting import *


#-------------------------------------------------------------------------
# selected sensor locations    
#-------------------------------------------------------------------------
with open('first_sensor_location.pickle') as f:
    x_max_indices_1, y_max_indices_1, Loc_1_x, Loc_1_y = pickle.load(f)
    
with open('second_sensor_location.pickle') as s:
    x_max_indices_2, y_max_indices_2, Loc_2_x, Loc_2_y = pickle.load(s)
    
with open('third_sensor_location.pickle') as t:
    x_max_indices_3, y_max_indices_3, Loc_3_x, Loc_3_y = pickle.load(t)

selected_sensor_locations = [np.array([x_max_indices_1[0],y_max_indices_1[0]]), np.array([x_max_indices_2[0],y_max_indices_2[0]]), np.array([x_max_indices_3[0],y_max_indices_3[0]])]

#-------------------------------------------------------------------------
# generate sensor locations list
#-------------------------------------------------------------------------

sensor_locations_list = [0]*num_comp
sensor_locations_list[0] = selected_sensor_locations
num_sensors = 3
sensor_locations = sensor_locations_generator(y_resolution, x_resolution, num_sensors*num_comp)
for i in range(1, num_comp):
    sensor_locations_list[i] = [sensor_locations[i], sensor_locations[i+1], sensor_locations[i+2]]


with open('sensor_locations_list.pickle', 'w') as sensor:
    pickle.dump(sensor_locations_list, sensor)   

print "sensor locations done !"
