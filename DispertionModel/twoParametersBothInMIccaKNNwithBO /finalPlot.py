# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 09:41:35 2015

@author: xiaolin
"""

import pickle
import matplotlib.pyplot as plt
import numpy as np
from simulation_setting import *

with open('initial_smps.pickle') as i:
    initial_smps = pickle.load(i)
    
real_release_source_list = initial_smps[0][100:(100 + num_cases)]
real_wind_dir_list = initial_smps[1][100:(100 + num_cases)]

with open('X_list_list_list.pickle') as f:
    X_list_list_list = pickle.load(f)
    
with open('ENT_list_list.pickle') as s:
    ENT_list_list = pickle.load(s)

with open('ENTboth_list_list.pickle') as sb:
    ENTboth_list_list = pickle.load(sb) 
    
with open('bo/BO_ENT_list.pickle') as B:
    BO_ENT_list = pickle.load(B)    
    
with open('bo/BO_ENTboth_list.pickle') as BOth:
    BO_ENTboth_list = pickle.load(BOth)  

# compare entropies
average_ENT = np.zeros((num_comp, num_steps))
for i in range(0, num_comp):
    for j in range(0, num_cases):
        average_ENT[i] = average_ENT[i] + ENT_list_list[j][i]
    average_ENT[i] = average_ENT[i]/num_cases

average_rand_ENT = average_ENT[1:,].mean(axis = 0)

average_BO_ENT = np.zeros(num_steps);
for j in range(0, num_cases):
    average_BO_ENT = average_BO_ENT + BO_ENT_list[j]
    
average_BO_ENT = average_BO_ENT/num_cases
    
# plot entropies
plt.figure()
for i in range(2, num_comp):
    plt.plot(time_sequence/60, average_ENT[i], 'b:')
plt.plot(time_sequence/60, average_ENT[1], 'b:',label="Random placement")
plt.plot(time_sequence/60, average_ENT[0],'rD',label="MI via grids", markersize=5)
#plt.plot(time_sequence/60, average_rand_ENT, 'bo',label="Average",markersize=10) 
plt.plot(time_sequence/60, average_BO_ENT, 'gD',label="MI via Bayes Opt",markersize=5)
plt.legend(loc="upper right", shadow=True, fancybox=True)
plt.xticks(np.linspace(0,30,4))
plt.axis([0, 30, 1, 10])
plt.yticks([1, 4, 7, 10])
plt.xlabel("Time (min)")
plt.ylabel("Entropy")
plt.savefig('Comparison100.eps',format='eps', dpi=1200)
plt.savefig('Comparison100.pdf',format='pdf', dpi=1200)

# compare joint entropies
average_ENTboth = np.zeros((num_comp, num_steps))
for i in range(0, num_comp):
    for j in range(0, num_cases):
        average_ENTboth[i] = average_ENTboth[i] + ENTboth_list_list[j][i]
    average_ENTboth[i] = average_ENTboth[i]/num_cases

average_rand_ENTboth = average_ENTboth[2:,].mean(axis = 0)

average_BO_ENTboth = np.zeros(num_steps);
for j in range(0, num_cases):
    average_BO_ENTboth = average_BO_ENTboth + BO_ENTboth_list[j]
    
average_BO_ENTboth = average_BO_ENTboth/num_cases
    
# plot entropies
plt.figure()
for i in range(2, num_comp):
    plt.plot(time_sequence/60, average_ENTboth[i], 'b:')
plt.plot(time_sequence/60, average_ENTboth[1], 'b:',label="Random placement")
plt.plot(time_sequence/60, average_ENTboth[0],'rD',label="MI via grids", markersize=5)
#plt.plot(time_sequence/60, average_rand_ENTboth, 'bo',label="Average",markersize=10) 
plt.plot(time_sequence/60, average_BO_ENTboth, 'gD',label="MI via Bayes Opt",markersize=5)
plt.legend(loc="upper right", shadow=True, fancybox=True)
plt.xticks(np.linspace(0,30,4))
plt.yticks([-2, 3, 8, 13])
plt.axis([0, 30,-2 , 13])
plt.xlabel("Time (min)")
plt.ylabel("Joint entropy")
plt.savefig('ComparisonBoth100.eps',format='eps', dpi=1200)
plt.savefig('ComparisonBoth100.pdf',format='pdf', dpi=1200)


showcase = 0

plt.figure()
plt.subplot(2,2,1)
ax = plt.hist(X_list_list_list[showcase][0][0][0,:]/1000.0, 30)
plt.plot([real_release_source_list[showcase]/1000.0, real_release_source_list[showcase]/1000.0],[0, ax[0].max()],'r')
#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'both')
plt.xticks([-3,0, -1.0,  1.0, 3.0])
plt.xlim([-3,3])
plt.yticks([0, 20, 40, 60])

#plt.yticks([0, 30, 60,  90])#plt.subplot(2,3,2).set_xlim(-3e3, 3e3)
plt.xlabel(" Release location (km)")
plt.ylabel("Frequency")
plt.title('Initial distribution')


plt.subplot(2,2,2)
ax = plt.hist(X_list_list_list[showcase][0][10][0,:]/1000.0, 100)
plt.plot([real_release_source_list[showcase]/1000.0, real_release_source_list[showcase]/1000.0],[0, ax[0].max()],'r')
plt.xlabel(" Release location (km)")
plt.ylabel("Frequency")
#plt.xticks([-1, 0,  1])
plt.xticks([-1.4, -1.2,  -1.0, -0.8])
plt.xlim([-1.4,-0.8])

plt.yticks([0, 50, 100, 150])

#plt.yticks([0, 30, 60,  90])#plt.subplot(2,3,2).set_xlim(-3e3, 3e3)
#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'y')

#plt.locator_params(nbins=4)
plt.title( 't = 10 min')


plt.subplot(2,2,3)
ax =plt.hist(X_list_list_list[showcase][0][20][0,:]/1000.0, 100)
plt.plot([real_release_source_list[showcase]/1000.0, real_release_source_list[showcase]/1000.0],[0, ax[0].max()],'r')
plt.xlabel(" Release location (km)")
plt.ylabel("Frequency")
#plt.xticks([-1, 0,  1])
#plt.xticks([-1, 0,  1])
plt.xticks([-1.4, -1.2,  -1.0, -0.8])
plt.xlim([-1.4,-0.8])
plt.yticks([0, 20, 40, 60])

#plt.yticks([0, 30, 60,  90])#plt.subplot(2,3,2).set_xlim(-3e3, 3e3)
#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'both')
#plt.locator_params(nbins=4)
plt.xlabel(" Release location (km)")
plt.ylabel("Frequency")
plt.title('t = 20 min')

plt.subplot(2,2,4)
ax =plt.hist(X_list_list_list[showcase][0][30][0,:]/1000.0, 100)
plt.plot([real_release_source_list[showcase]/1000.0, real_release_source_list[showcase]/1000.0],[0, ax[0].max()],'r')
plt.xticks([-1.4, -1.2,  -1.0, -0.8])
plt.xlim([-1.4,-0.8])
#plt.xticks([-1, 0,  1])
#plt.xticks([-1, 0,  1])

plt.yticks([0, 20, 40, 60])

#plt.yticks([0, 30, 60,  90])#plt.subplot(2,3,2).set_xlim(-3e3, 3e3)
#plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'both')
#plt.locator_params(nbins=4)
plt.xlabel(" Release location (km)")
plt.ylabel("Frequency")
plt.title('t = 30 min')

'''
plt.subplot(2,3,5)
plt.hist(X_list_list_list[showcase][0][12][0,:],100)
plt.ticklabel_format( scilimits = (0,0) , axis = 'both')
plt.locator_params(nbins=4)

plt.title('the 12th update ')


plt.subplot(2,3,6)
plt.hist(X_list_list_list[showcase][0][16][0,:],100)
plt.ticklabel_format(style = 'sci', scilimits = (0,0) , axis = 'both')
plt.locator_params(nbins=4)

plt.title('the 16th update ')
'''
plt.tight_layout() 

plt.savefig('EnKFupdate100.eps',format='eps', dpi=1200)
plt.savefig('EnKFupdate100.pdf',format='pdf', dpi=1200)


plt.figure()
plt.subplot(2,2,1)
ax = plt.hist(X_list_list_list[showcase][0][0][1,:], 30)
plt.plot([real_wind_dir_list[showcase], real_wind_dir_list[showcase]],[0, ax[0].max()],'r')
plt.xlabel(" Wind direction (rad)")
plt.ylabel("Frequency")
plt.title('Initial distribution')
plt.xticks([-0.6, -0.2,  0.2, 0.6])
plt.xlim([-0.6,0.6])
plt.yticks([0, 40, 80, 120])
#plt.ticklabel_format( scilimits = (0,0) , axis = 'both')
#plt.locator_params(nbins=4)

plt.subplot(2,2,2)
ax = plt.hist(X_list_list_list[showcase][0][10][1,:], 100)
plt.plot([real_wind_dir_list[showcase], real_wind_dir_list[showcase]],[0, ax[0].max()],'r')
plt.title( 't = 10 min')
plt.xlabel(" Wind direction (rad)")
plt.ylabel("Frequency")
plt.xticks([-0.2, -0.1,  0., 0.1])
plt.xlim([-0.2,0.1])
plt.yticks([0, 40, 80, 120])
#plt.ticklabel_format( scilimits = (0,0) , axis = 'both')
#plt.locator_params(nbins=4)

plt.subplot(2,2,3)
ax =plt.hist(X_list_list_list[showcase][0][20][1,:], 100)
plt.plot([real_wind_dir_list[showcase], real_wind_dir_list[showcase]],[0, ax[0].max()],'r')
plt.xlabel(" Wind direction (rad)")
plt.ylabel("Frequency")
plt.title('t = 20 min')
plt.xticks([-0.2, -0.1,  0., 0.1])
plt.xlim([-0.2,0.1])
plt.yticks([0, 20, 40, 60])
#plt.ticklabel_format( scilimits = (0,0) , axis = 'both')
#plt.locator_params(nbins=4)

plt.subplot(2,2,4)
ax =plt.hist(X_list_list_list[showcase][0][30][1,:], 100)
plt.plot([real_wind_dir_list[showcase], real_wind_dir_list[showcase]],[0, ax[0].max()],'r')
plt.xlabel(" Wind direction (rad)")
plt.ylabel("Frequency")
plt.title('t = 30 min')
plt.xticks([-0.2, -0.1,  0., 0.1])
plt.xlim([-0.2,0.1])
plt.yticks([0, 20, 40, 60])
#plt.ticklabel_format( scilimits = (0,0) , axis = 'both')
#plt.locator_params(nbins=4)
plt.tight_layout() 

plt.figure()
plt.plot(X_list_list_list[showcase][0][30][1,:], X_list_list_list[showcase][0][30][0,:]/1000.0,'.')
plt.xlabel(" Wind direction (rad)")
plt.ylabel("Release Location (km)")
plt.title('Posterior joint distribution')
#plt.axis([-5e-5, 5e-5, -0.4, -0.1])

# plot MI map
'''
with open('first_sensor_location.pickle') as ff:
    x_max_indices_1, y_max_indices_1, Loc_1_x, Loc_1_y = pickle.load(ff)
    
with open('second_sensor_location.pickle') as ss:
    x_max_indices_2, y_max_indices_2, Loc_2_x, Loc_2_y = pickle.load(ss)

with open('third_sensor_location.pickle') as tt:
    x_max_indices_3, y_max_indices_3, Loc_3_x, Loc_3_y = pickle.load(tt)
    
with open('BO_first_sensor_location.pickle') as ff:
    BO_Loc_1_x, BO_Loc_1_y = pickle.load(ff)
    
with open('BO_second_sensor_location.pickle') as ss:
    BO_Loc_2_x, BO_Loc_2_y = pickle.load(ss)

with open('BO_third_sensor_location.pickle') as tt:
    BO_Loc_3_x, BO_Loc_3_y = pickle.load(tt)
    
with open('MI_map_1.pickle') as m1:
    MI_map_1 = pickle.load(m1)
with open('MI_map_2.pickle') as m2:
    MI_map_2 = pickle.load(m2)
with open('MI_map_3.pickle') as m3:
    MI_map_3 = pickle.load(m3)
    
plt.figure()
plt.subplot(1,3,1)    
plt.pcolor(x_grid/1000, y_grid/1000, MI_map_1, cmap='rainbow', vmin=MI_map_1.min(), vmax=MI_map_1.max())
plt.title('Sensor 1')
plt.scatter(np.array([Loc_1_x/1000]), np.array([Loc_1_y/1000]),s = 60, c = 'b', marker = 'D')
plt.scatter(BO_Loc_1_x, BO_Loc_1_y, s = 60, c='g', marker ='D')
plt.locator_params(nbins=4)
# set the limits of the plot to the limits of the data
plt.axis([x_grid.min()/1000, x_grid.max()/1000, y_grid.min()/1000, y_grid.max()/1000])
plt.ylabel('y (km)')
plt.xlabel('x (km)')

plt.subplot(1,3,2)    
plt.pcolor(x_grid/1000, y_grid/1000, MI_map_2, cmap='rainbow', vmin=MI_map_2.min(), vmax=MI_map_2.max())
plt.title('Sensor 1&2')
plt.scatter(np.array([Loc_1_x/1000, Loc_2_x/1000]), np.array([Loc_1_y/1000, Loc_2_y/1000]),s = 60, c = 'b', marker = 'D')
plt.scatter(np.array([BO_Loc_1_x, BO_Loc_2_x]), np.array([BO_Loc_1_y, BO_Loc_2_y]),s = 60, c='g', marker ='D')

# set the limits of the plot to the limits of the data
plt.locator_params(nbins=4)
plt.axis([x_grid.min()/1000, x_grid.max()/1000, y_grid.min()/1000, y_grid.max()/1000])
plt.xlabel('x (km)')

plt.subplot(1,3,3)    
plt.pcolor(x_grid/1000, y_grid/1000, MI_map_3, cmap='rainbow', vmin=MI_map_3.min(), vmax=MI_map_3.max())
plt.title('Sensor 1,2&3')
plt.scatter(np.array([Loc_1_x/1000, Loc_2_x/1000, Loc_3_x/1000]), np.array([Loc_1_y/1000, Loc_2_y/1000, Loc_3_y/1000]),s = 60, c = 'b', marker = 'D')
plt.scatter(np.array([BO_Loc_1_x, BO_Loc_2_x, BO_Loc_3_x]), np.array([BO_Loc_1_y, BO_Loc_2_y, BO_Loc_3_y]),s = 60, c='g', marker ='D')

plt.locator_params(nbins=4)
# set the limits of the plot to the limits of the data
plt.axis([x_grid.min()/1000, x_grid.max()/1000, y_grid.min()/1000, y_grid.max()/1000])
plt.xlabel('x (km)')

plt.tight_layout() 

plt.savefig('sensors.eps',format='eps', dpi=1200)
plt.savefig('sensors.pdf',format='pdf', dpi=1200)
'''