# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 21:21:50 2015

@author: xiaolin
"""
from DispersionModel import *

from utils import *
import numpy as np
import pickle
import matplotlib.pyplot as plt
from simulation_setting import *
import math

with open('c_dispersion_list_grids.pickle') as c:
    c_dispersion_list_grids = pickle.load(c)
    
with open('initial_smps.pickle') as i:
    initial_smps = pickle.load(i)

#------------------------------------------------------------------------------
# select first sensor position
#------------------------------------------------------------------------------
my_chain_1 = np.zeros((num_smps, (num_steps-1) + 2)) # 3 or 4 ???
my_split_1 = [num_steps-1,2]
MI_map_1 = x_grid*0

for i in range(0, y_resolution):
    for j in range(0, x_resolution):
        print "computing mutual information at (i, j):   ", (i,j)
        row = 0
        for r in range(0, num_smps):
            d_list = []
            for t in range(1, num_steps): 
                d_list.append(c_dispersion_list_grids[r][t][i][j])                                          
            d_list.append(initial_smps[0][r])
            d_list.append(initial_smps[1][r])
            my_chain_1[row] = np.array(d_list)        
            row = row + 1            
        MI_map_1[i][j] = MI_CCA(my_chain_1, my_split_1)  

# to avoid nan        
for i in range(0, y_resolution):
    for j in range(0, x_resolution):
        if MI_map_1[i][j]== -1e10:
            MI_map_1[i][j] = 1e10
            
MI_map_1_min = MI_map_1.min()
      
for i in range(0, y_resolution):
    for j in range(0, x_resolution):
        if MI_map_1[i][j] == 1e10:
            MI_map_1[i][j] = MI_map_1_min
            
with open('MI_map_1.pickle', 'w') as m:
    pickle.dump(MI_map_1, m)

                
[x_max_indices_1, y_max_indices_1] = np.where(MI_map_1 == MI_map_1.max())
Loc_1_x = x_axis[y_max_indices_1[0]]
Loc_1_y = y_axis[x_max_indices_1[0]]

with open('first_sensor_location.pickle', 'w') as f:
    pickle.dump([x_max_indices_1, y_max_indices_1, Loc_1_x, Loc_1_y], f)
    
plt.figure()
plt.pcolor(x_grid, y_grid, MI_map_1, cmap='rainbow', vmin=MI_map_1.min(), vmax=MI_map_1.max())
plt.colorbar()
plt.title('first sensor')
plt.scatter(Loc_1_x, Loc_1_y,s = 60, c = 'b', marker = 'D')
# set the limits of the plot to the limits of the data
plt.axis([x_grid.min(), x_grid.max(), y_grid.min(), y_grid.max()]) 

plt.savefig('first_sensor.eps',format='eps', dpi=1200)

#import second_sensor_selection