from DispersionModel import *
from utils import *
import numpy as np
import pickle
import matplotlib.pyplot as plt
#------------------------------------------------------------------------------
# prior information
#------------------------------------------------------------------------------
release_range = [-3000., 3000.] #uniform distribution
wind_dir_range = [0., np.pi/18] #Gaussian distribution N(0, pi/18^2)
num_para = 2

#------------------------------------------------------------------------------
# observation noise
#------------------------------------------------------------------------------
obs_var = 1e-10 #variance of the observation noise

#------------------------------------------------------------------------------
# samples in Monte Carlo methods
#------------------------------------------------------------------------------
num_smps = 500
initial_release_smps = np.tile(release_range[0], (1, num_smps)) + np.tile(release_range[1], (1, num_smps))*np.random.rand(1, num_smps)
initial_wind_dir_smps = np.tile(wind_dir_range[0], (1, num_smps)) + np.tile(wind_dir_range[1], (1, num_smps))*np.random.randn(1, num_smps)
initial_smps = np.vstack((initial_release_smps, initial_wind_dir_smps))

#------------------------------------------------------------------------------
# real parameters
#------------------------------------------------------------------------------
mass = 10000
real_wind_spd = 5.
real_wind_dir = 0.
real_release_source = [0.,0.]
p = 0.466
q = 0.866

#------------------------------------------------------------------------------
# time setting
#------------------------------------------------------------------------------
time_sequence = np.linspace(0,3600, 121)
time_interval = time_sequence[1] - time_sequence[0]
start_time_sequence = np.linspace(0,900,16 )
num_steps = len(time_sequence)

#------------------------------------------------------------------------------
# domain
#------------------------------------------------------------------------------
x_resolution = 26
y_resolution = 51
x_axis = np.linspace(0, 12000, x_resolution)
y_axis = np.linspace(-12000, 12000, y_resolution)
[x_grid, y_grid] = np.meshgrid(x_axis, y_axis)

#------------------------------------------------------------------------------
# run simulations
#------------------------------------------------------------------------------
c_dispersion_list_grids = [0]*num_smps

for i in range(0, num_smps):
    c_dispersion_list_grids[i] = dispersion(mass, real_wind_spd, initial_smps[1][i],p, q, initial_smps[0][i], start_time_sequence, time_sequence, time_interval, x_grid, y_grid)

with open('c_disperdion_list_grids.pickle', 'w') as f:
    pickle.dump(c_dispersion_list_grids, f)
    
#------------------------------------------------------------------------------
# select first sensor position
#------------------------------------------------------------------------------
my_chain_1 = np.zeros((num_smps*num_steps, 4)) # 3 or 4 ???
my_split_1 = [2,2]
MI_map_1 = x_grid*0

for i in range(0, y_resolution):
    for j in range(0, x_resolution):
        row = 0
        for r in range(0, num_smps):
            for t in range(0, num_steps):               
                my_chain_1[row] = np.array([c_dispersion_list_grids[r][t][i][j],time_sequence[t],initial_smps[0][r],initial_smps[1][r]])        
                row = row + 1
        MI_map_1[i][j] = MI_GaussApprox(my_chain_1, my_split_1)
        
with open('MI_map_1.pickle', 'w') as f:
    pickle.dump(MI_map_1, f)
        
[x_max_indices_1, y_max_indices_1] = np.where(MI_map_1 == MI_map_1.max())
Loc_1_x = x_axis[y_max_indices_1[0]]
Loc_1_y = y_axis[x_max_indices_1[0]]

plt.figure()
plt.pcolor(x_grid, y_grid, MI_map_1, cmap='rainbow', vmin=MI_map_1.min(), vmax=MI_map_1.max())
plt.colorbar()
plt.title('first sensor')
plt.scatter(Loc_1_x, Loc_1_y,s = 60, c = 'b', marker = 'D')
# set the limits of the plot to the limits of the data
plt.axis([x_grid.min(), x_grid.max(), y_grid.min(), y_grid.max()]) 

plt.show()
plt.savefig('first_senor.eps',format='eps', dpi=1200)

  
#------------------------------------------------------------------------------
# select second sensor position
#------------------------------------------------------------------------------
my_chain_2 = np.zeros((num_smps*num_steps, 5)) 
my_split_2 = [3,2]
MI_map_2 = x_grid*0

for i in range(0, y_resolution):
    for j in range(0, x_resolution):
        row = 0
        for r in range(0, num_smps):
            for t in range(0, num_steps):               
                my_chain_2[row] = np.array([c_dispersion_list_grids[r][t][x_max_indices_1[0]][y_max_indices_1[0]], c_dispersion_list_grids[r][t][i][j],time_sequence[t],initial_smps[0][r],initial_smps[1][r]])        
                row = row + 1
        MI_map_2[i][j] = MI_GaussApprox(my_chain_2, my_split_2)
MI_map_2[x_max_indices_1[0]][y_max_indices_1[0]] = 0 

with open('MI_map_2.pickle', 'w') as f:
    pickle.dump(MI_map_2, f)
    
[x_max_indices_2, y_max_indices_2] = np.where(MI_map_2 == MI_map_2.max())
Loc_2_x = x_axis[y_max_indices_2[0]]
Loc_2_y = y_axis[x_max_indices_2[0]]

plt.figure()
plt.pcolor(x_grid, y_grid, MI_map_2, cmap='rainbow', vmin=MI_map_2.min(), vmax=MI_map_2.max())
plt.colorbar()
plt.title('second sensor')
plt.scatter(np.array([Loc_1_x, Loc_2_x]), np.array([Loc_1_y, Loc_2_y]),s = 60, c = 'b', marker = 'D')
# set the limits of the plot to the limits of the data
plt.axis([x_grid.min(), x_grid.max(), y_grid.min(), y_grid.max()])

plt.show()
plt.savefig('second_puff.eps',format='eps', dpi=1200)


#------------------------------------------------------------------------------
# select third sensor position
#------------------------------------------------------------------------------
my_chain_3 = np.zeros((num_smps*num_steps, 6)) 
my_split_3 = [4,2]
MI_map_3 = x_grid*0

for i in range(0, y_resolution):
    for j in range(0, x_resolution):
        row = 0
        for r in range(0, num_smps):
            for t in range(0, num_steps):               
                my_chain_3[row] = np.array([c_dispersion_list_grids[r][t][x_max_indices_1[0]][y_max_indices_1[0]], c_dispersion_list_grids[r][t][x_max_indices_2[0]][y_max_indices_2[0]], c_dispersion_list_grids[r][t][i][j],time_sequence[t],initial_smps[0][r],initial_smps[1][r]])        
                row = row + 1
        MI_map_3[i][j] = MI_GaussApprox(my_chain_3, my_split_3)
MI_map_3[x_max_indices_1[0]][y_max_indices_1[0]] = 0 
MI_map_3[x_max_indices_2[0]][y_max_indices_2[0]] = 0 

with open('MI_map_3.pickle', 'w') as f:
    pickle.dump(MI_map_3, f)
    
[x_max_indices_3, y_max_indices_3] = np.where(MI_map_3 == MI_map_3.max())
Loc_3_x = x_axis[y_max_indices_3[0]]
Loc_3_y = y_axis[x_max_indices_3[0]]

plt.figure()
plt.pcolor(x_grid, y_grid, MI_map_3, cmap='rainbow', vmin=MI_map_3.min(), vmax=MI_map_3.max())
plt.colorbar()
plt.title('third sensor')
plt.scatter(np.array([Loc_1_x, Loc_2_x, Loc_3_x]), np.array([Loc_1_y, Loc_2_y, Loc_3_y]),s = 60, c = 'b', marker = 'D')
# set the limits of the plot to the limits of the data
plt.axis([x_grid.min(), x_grid.max(), y_grid.min(), y_grid.max()])

plt.show()
plt.savefig('third_puff.eps',format='eps', dpi=1200)
        

    

