# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 09:41:35 2015

@author: xiaolin
"""

import pickle
import matplotlib.pyplot as plt
import numpy as np
from simulation_setting import *


# plot MI map

with open('first_sensor_location.pickle') as ff:
    x_max_indices_1, y_max_indices_1, Loc_1_x, Loc_1_y = pickle.load(ff)
    
with open('second_sensor_location.pickle') as ss:
    x_max_indices_2, y_max_indices_2, Loc_2_x, Loc_2_y = pickle.load(ss)

with open('third_sensor_location.pickle') as tt:
    x_max_indices_3, y_max_indices_3, Loc_3_x, Loc_3_y = pickle.load(tt)
    
with open("bo/BO_first_sensor_location.pickle") as ff:
    BO_Loc_1_x, BO_Loc_1_y = pickle.load(ff)
    
with open("bo/BO_second_sensor_location.pickle") as ss:
    BO_Loc_2_x, BO_Loc_2_y = pickle.load(ss)

with open("bo/BO_third_sensor_location.pickle") as tt:
    BO_Loc_3_x, BO_Loc_3_y = pickle.load(tt)
    
with open('MI_map_1.pickle') as m1:
    MI_map_1 = pickle.load(m1)
with open('MI_map_2.pickle') as m2:
    MI_map_2 = pickle.load(m2)
with open('MI_map_3.pickle') as m3:
    MI_map_3 = pickle.load(m3)
    
plt.figure()
plt.subplot(1,3,1)    
plt.pcolor(x_grid/1000, y_grid/1000, MI_map_1, cmap='rainbow', vmin=MI_map_1.min(), vmax=MI_map_1.max())
plt.title('Sensor 1')
plt.scatter(np.array([Loc_1_x/1000]), np.array([Loc_1_y/1000]),s = 60, c = 'b', marker = 'D')
plt.scatter(BO_Loc_1_x/1000, BO_Loc_1_y/1000, s = 60, c='g', marker ='D')
plt.locator_params(nbins=4)
# set the limits of the plot to the limits of the data
plt.axis([x_grid.min()/1000, x_grid.max()/1000, y_grid.min()/1000, y_grid.max()/1000])
plt.ylabel('y (km)')
plt.xlabel('x (km)')

plt.subplot(1,3,2)    
plt.pcolor(x_grid/1000, y_grid/1000, MI_map_2, cmap='rainbow', vmin=MI_map_2.min(), vmax=MI_map_2.max())
plt.title('Sensor 1&2')
plt.scatter(np.array([Loc_1_x/1000, Loc_2_x/1000]), np.array([Loc_1_y/1000, Loc_2_y/1000]),s = 60, c = 'b', marker = 'D')
plt.scatter(np.array([BO_Loc_1_x/1000, BO_Loc_2_x/1000]), np.array([BO_Loc_1_y/1000, BO_Loc_2_y/1000]),s = 60, c='g', marker ='D')

# set the limits of the plot to the limits of the data
plt.locator_params(nbins=4)
plt.axis([x_grid.min()/1000, x_grid.max()/1000, y_grid.min()/1000, y_grid.max()/1000])
plt.xlabel('x (km)')

plt.subplot(1,3,3)    
plt.pcolor(x_grid/1000, y_grid/1000, MI_map_3, cmap='rainbow', vmin=MI_map_3.min(), vmax=MI_map_3.max())
plt.title('Sensor 1,2&3')
plt.scatter(np.array([Loc_1_x/1000, Loc_2_x/1000, Loc_3_x/1000]), np.array([Loc_1_y/1000, Loc_2_y/1000, Loc_3_y/1000]),s = 60, c = 'b', marker = 'D')
plt.scatter(np.array([BO_Loc_1_x/1000, BO_Loc_2_x/1000, BO_Loc_3_x/1000]), np.array([BO_Loc_1_y/1000, BO_Loc_2_y/1000, BO_Loc_3_y/1000]),s = 60, c='g', marker ='D')

plt.locator_params(nbins=4)
# set the limits of the plot to the limits of the data
plt.axis([x_grid.min()/1000, x_grid.max()/1000, y_grid.min()/1000, y_grid.max()/1000])
plt.xlabel('x (km)')

plt.tight_layout() 

plt.savefig('sensors.eps',format='eps', dpi=1200)
plt.savefig('sensors.pdf',format='pdf', dpi=1200)